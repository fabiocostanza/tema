=== Tema ===
Tags: custom-colors, theme-options, translation-ready, tema-custom
Requires at least: 4.9.6
Tested up to: WordPress 5.2.3
Stable tag: 1.0.0
License: Free

Tema personalizzato per WordPress.

== Description ==
Tema personalizzato per WordPress.

== Changelog ==

= 1.0.0 =
* Released: Initial release
