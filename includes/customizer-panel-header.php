<?php

/* Header Panel */
FLCustomizer::add_panel('fl-header', array(
	'title'    => _x( 'Header', 'Customizer panel title.', 'tema' ),
	'sections' => array(

		/* Top Bar Layout Section */
		'fl-topbar-layout' => array(
			'title'   => _x( 'Top Bar Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Top Bar Layout */
				'fl-topbar-layout'      => array(
					'setting' => array(
						'default' => 'none',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Layout', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'   => __( 'None', 'tema' ),
							'1-col'  => __( '1 Column', 'tema' ),
							'2-cols' => __( '2 Columns', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-topbar-line1'       => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Top Bar Column 1 Layout */
				'fl-topbar-col1-layout' => array(
					'setting' => array(
						'default' => 'text',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label'   => sprintf( _x( 'Column %d Layout', '%d stands for column order number.', 'tema' ), 1 ),
						'type'    => 'select',
						'choices' => array(
							'text'        => __( 'Text', 'tema' ),
							'text-social' => __( 'Text &amp; Social Icons', 'tema' ),
							'menu'        => __( 'Menu', 'tema' ),
							'menu-social' => __( 'Menu &amp; Social Icons', 'tema' ),
							'social'      => __( 'Social Icons', 'tema' ),
						),
					),
				),

				/* Top Bar Column 1 Text */
				'fl-topbar-col1-text'   => array(
					'setting' => array(
						'default'   => '',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label' => sprintf( _x( 'Column %d Text', '%d stands for column order number.', 'tema' ), 1 ),
						'type'  => 'textarea',
					),
				),

				/* Line */
				'fl-topbar-line2'       => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Top Bar Column 2 Layout */
				'fl-topbar-col2-layout' => array(
					'setting' => array(
						'default' => 'menu',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label'   => sprintf( _x( 'Column %d Layout', '%d stands for column order number.', 'tema' ), 2 ),
						'type'    => 'select',
						'choices' => array(
							'text'        => __( 'Text', 'tema' ),
							'text-social' => __( 'Text &amp; Social Icons', 'tema' ),
							'menu'        => __( 'Menu', 'tema' ),
							'menu-social' => __( 'Menu &amp; Social Icons', 'tema' ),
							'social'      => __( 'Social Icons', 'tema' ),
						),
					),
				),

				/* Top Bar Column 2 Text */
				'fl-topbar-col2-text'   => array(
					'setting' => array(
						'default'   => '',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label' => sprintf( _x( 'Column %d Text', '%d stands for column order number.', 'tema' ), 2 ),
						'type'  => 'textarea',
					),
				),
			),
		),

		/* Top Bar Style Section */
		'fl-topbar-style'  => array(
			'title'   => _x( 'Top Bar Style', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Top Bar Background Color */
				'fl-topbar-bg-color'      => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Top Bar Background Opacity */
				'fl-topbar-bg-opacity'    => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Top Bar Background Gradient */
				'fl-topbar-bg-gradient'   => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Gradient', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'0' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'1' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Top Bar Background Image */
				'fl-topbar-bg-image'      => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Top Bar Background Repeat */
				'fl-topbar-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Top Bar Background Position */
				'fl-topbar-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Top Bar Background Attachment */
				'fl-topbar-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Top Bar Background Size */
				'fl-topbar-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),

				/* Top Bar Text Color */
				'fl-topbar-text-color'    => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Text Color', 'tema' ),
					),
				),

				/* Top Bar Link Color */
				'fl-topbar-link-color'    => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Link Color', 'tema' ),
					),
				),

				/* Top Bar Hover Color */
				'fl-topbar-hover-color'   => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
			),
		),

		/* Header Layout Section */
		'fl-header-layout' => array(
			'title'   => _x( 'Header Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Header Layout */
				'fl-header-layout'            => array(
					'setting' => array(
						'default' => 'right',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Layout', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'                 => __( 'None', 'tema' ),
							'bottom'               => __( 'Nav Bottom', 'tema' ),
							'right'                => __( 'Nav Right', 'tema' ),
							'left'                 => __( 'Nav Left', 'tema' ),
							'centered'             => __( 'Nav Centered', 'tema' ),
							'centered-inline-logo' => __( 'Nav Centered + Inline Logo', 'tema' ),
							'vertical-left'        => __( 'Nav Vertical Left', 'tema' ),
							'vertical-right'       => __( 'Nav Vertical Right', 'tema' ),
						),
					),
				),

				/* Inline Logo Side */
				'fl-inline-logo-side'         => array(
					'setting' => array(
						'default' => 'right',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Inline Logo Position', 'tema' ),
						'description' => __( 'The inline logo will appear on the left or right side of odd menu items.', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'left'  => __( 'Left', 'tema' ),
							'right' => __( 'Right', 'tema' ),
						),
					),
				),

				/* Vertical Header Width */
				'fl-vertical-header-width'    => array(
					'setting' => array(
						'default'   => '230',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Vertical Nav Width', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 200,
							'max'  => 400,
							'step' => 1,
						),
					),
				),

				/* Header Padding */
				'fl-header-padding'           => array(
					'setting' => array(
						'default'   => '30',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Padding', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Fixed Header */
				'fl-fixed-header'             => array(
					'setting' => array(
						'default' => 'fadein',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Fixed Header', 'tema' ),
						'description' => __( 'Show a fixed header as the page is scrolled.', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'hidden' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'fadein' => _x( 'Fade In', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'shrink' => _x( 'Shrink', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'fixed'  => _x( 'Fixed', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Hide Header Until Scroll */
				'fl-hide-until-scroll-header' => array(
					'setting' => array(
						'default' => 'disable',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Hide Header Until Scroll', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'disable' => __( 'Disabled', 'tema' ),
							'enable'  => __( 'Enabled', 'tema' ),
						),
					),
				),

				/* Scroll Distance for Hide Header Until Scroll */
				'fl-scroll-distance'          => array(
					'setting' => array(
						'default' => '200',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Scroll Distance', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 50,
							'max'  => 1000,
							'step' => 1,
						),
					),
				),

				/* Line */
				'fl-header-line1'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Header Content Layout */
				'fl-header-content-layout'    => array(
					'setting' => array(
						'default' => 'social-text',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Content Layout', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'        => __( 'None', 'tema' ),
							'text'        => __( 'Text', 'tema' ),
							'social'      => __( 'Social Icons', 'tema' ),
							'social-text' => __( 'Text &amp; Social Icons', 'tema' ),
						),
					),
				),

				/* Header Content Text */
				'fl-header-content-text'      => array(
					'setting' => array(
						'default' => 'Call Us! 1-800-555-5555',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( 'Content Text', 'tema' ),
						'type'  => 'textarea',
					),
				),
			),
		),

		/* Header Style Section */
		'fl-header-style'  => array(
			'title'   => _x( 'Header Style', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Header Background Color */
				'fl-header-bg-color'      => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Header Background Opacity */
				'fl-header-bg-opacity'    => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Nav Drop Shadow Size */
				'fl-nav-shadow-size'      => array(
					'setting' => array(
						'default' => '4',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Drop Shadow Size', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 75,
							'step' => 1,
						),
					),
				),

				/* Nav Shadow Color */
				'fl-nav-shadow-color'     => array(
					'setting' => array(
						'default' => '#cecece',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Drop Shadow Color', 'tema' ),
					),
				),

				/* Header Background Gradient */
				'fl-header-bg-gradient'   => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Gradient', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'0' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'1' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Header Background Image */
				'fl-header-bg-image'      => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Header Background Repeat */
				'fl-header-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Header Background Position */
				'fl-header-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Header Background Attachment */
				'fl-header-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Header Background Size */
				'fl-header-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),

				/* Header Text Color */
				'fl-header-text-color'    => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Text Color', 'tema' ),
					),
				),

				/* Header Link Color */
				'fl-header-link-color'    => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Link Color', 'tema' ),
					),
				),

				/* Header Hover Color */
				'fl-header-hover-color'   => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
			),
		),

		/* Header Logo Section */
		'fl-header-logo'   => array(
			'title'   => _x( 'Header Logo', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Logo Type */
				'fl-logo-type'                 => array(
					'setting' => array(
						'default' => 'text',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Logo Type', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'text'  => __( 'Text', 'tema' ),
							'image' => __( 'Image', 'tema' ),
						),
					),
				),

				/* Logo Image (Regular) */
				'fl-logo-image'                => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Logo Image (Regular)', 'tema' ),
					),
				),

				/* Logo Image (Retina) */
				'fl-logo-image-retina'         => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Logo Image (Retina)', 'tema' ),
					),
				),

				/* Sticky Header Logo */
				'fl-sticky-header-logo'        => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class'       => 'WP_Customize_Image_Control',
						'label'       => __( 'Fade In Header Logo', 'tema' ),
						'description' => __( 'Use a different logo when you have a Fade In header', 'tema' ),
					),
				),

				/* Sticky Header Logo (Retina) */
				'fl-sticky-header-logo-retina' => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Fade In Header Logo (Retina)', 'tema' ),
					),
				),

				/* Mobile Header Logo */
				'fl-mobile-header-logo'        => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class'       => 'WP_Customize_Image_Control',
						'label'       => __( 'Mobile Header Logo', 'tema' ),
						'description' => __( 'Use a different logo for mobile devices.', 'tema' ),
					),
				),

				/* Logo Text */
				'fl-logo-text'                 => array(
					'setting' => array(
						'default'   => get_bloginfo( 'name' ),
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( 'Logo Text', 'tema' ),
						'type'  => 'text',
					),
				),

				/* Logo Text */
				'fl-theme-tagline'             => array(
					'setting' => array(
						'default'   => false,
						'transport' => 'refresh',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( 'Show Tagline', 'tema' ),
						'type'  => 'checkbox',
					),
				),

				/* Logo Font Family */
				'fl-logo-font-family'          => array(
					'setting' => array(
						'default'   => 'Helvetica',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Family', 'tema' ),
						'type'    => 'font',
						'connect' => 'fl-logo-font-weight',
					),
				),

				/* Logo Font Weight */
				'fl-logo-font-weight'          => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Weight', 'tema' ),
						'type'    => 'font-weight',
						'connect' => 'fl-logo-font-family',
					),
				),

				/* Logo Font Size */
				'fl-logo-font-size'            => array(
					'setting' => array(
						'default'   => '30',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Size', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 14,
							'max'  => 72,
							'step' => 1,
						),
					),
				),

				/* Logo Top Spacing */
				'fl-header-logo-top-spacing'   => array(
					'setting' => array(
						'default'   => '50',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Logo Top Spacing', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 1,
							'max'  => 200,
							'step' => 1,
						),
					),
				),
			),
		),

		/* Nav Layout Section */
		'fl-nav-layout'    => array(
			'title'   => _x( 'Nav Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Nav Item Spacing */
				'fl-nav-item-spacing'           => array(
					'setting' => array(
						'default'   => '15',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Nav Item Spacing', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 5,
							'max'  => 30,
							'step' => 1,
						),
					),
				),

				/* Nav Menu Top Spacing */
				'fl-nav-menu-top-spacing'       => array(
					'setting' => array(
						'default'   => '30',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Nav Menu Top Spacing', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 1,
							'max'  => 200,
							'step' => 1,
						),
					),
				),

				/* Nav Item Align */
				'fl-nav-item-align'             => array(
					'setting' => array(
						'default' => 'left',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Nav Item Align', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left'   => __( 'Left', 'tema' ),
							'center' => __( 'Center', 'tema' ),
							'right'  => __( 'Right', 'tema' ),
						),
					),
				),

				/* Nav Search */
				'fl-header-nav-search'          => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Nav Search Icon', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'hidden'  => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Mobile Nav Toggle */
				'fl-mobile-nav-toggle'          => array(
					'setting' => array(
						'default' => 'button',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Responsive Nav Toggle', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'button' => __( 'Menu Button', 'tema' ),
							'icon'   => __( 'Hamburger Icon', 'tema' ),
						),
					),
				),

				/* Mobile Nav Text */
				'fl-mobile-nav-text'            => array(
					'setting' => array(
						'default' => _x( 'Menu', 'Mobile navigation toggle button text.', 'tema' ),
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Menu Button Text', 'tema' ),
						'type'        => 'text',
						'input_attrs' => array(
							'placeholder' => _x( 'Menu', 'Mobile navigation toggle button text.', 'tema' ),
						),
					),
				),

				/* Responsive Nav Breakpoint */
				'fl-nav-breakpoint'             => array(
					'setting' => array(
						'default' => 'mobile',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Responsive Nav Breakpoint', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'always'        => __( 'Always', 'tema' ),
							'medium-mobile' => __( 'Medium &amp; Small Devices Only', 'tema' ),
							'mobile'        => __( 'Small Devices Only', 'tema' ),
						),
					),
				),

				/* Responsive Nav Layout */
				'fl-nav-mobile-layout'          => array(
					'setting' => array(
						'default' => 'dropdown',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Responsive Nav Layout', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'dropdown'     => __( 'Dropdown', 'tema' ),
							'overlay'      => __( 'Flyout Overlay', 'tema' ),
							'push'         => __( 'Flyout Push', 'tema' ),
							'push-opacity' => __( 'Flyout Push with Opacity', 'tema' ),
						),
					),
				),

				'fl-nav-mobile-layout-position' => array(
					'setting' => array(
						'default' => 'left',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Responsive Nav Layout Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left'  => __( 'Left', 'tema' ),
							'right' => __( 'Right', 'tema' ),
						),
					),
				),

				/* Submenu Toggle */
				'fl-nav-submenu-toggle'         => array(
					'setting' => array(
						'default' => 'disable',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Responsive Submenu Toggle', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'disable' => __( 'Disabled', 'tema' ),
							'enable'  => __( 'Enabled', 'tema' ),
						),
					),
				),

				/* Responsive Collapse */
				'fl-nav-collapse-menu'          => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Responsive Collapse', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'1' => __( 'Yes', 'tema' ),
							'0' => __( 'No', 'tema' ),
						),
						'description' => __( 'Only allow one menu item at a time to be expanded?', 'tema' ),
					),
				),
			),
		),

		/* Nav Style Section */
		'fl-nav-style'     => array(
			'title'   => _x( 'Nav Style', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Submenu Indicator */
				'fl-nav-submenu-indicator' => array(
					'setting' => array(
						'default' => 'disable',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Submenu Indicator', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'disable' => __( 'Disabled', 'tema' ),
							'enable'  => __( 'Enabled', 'tema' ),
						),
					),
				),

				/* Nav Background Color */
				'fl-nav-bg-color'          => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Nav Background Opacity */
				'fl-nav-bg-opacity'        => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Nav Background Gradient */
				'fl-nav-bg-gradient'       => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Gradient', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'0' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'1' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Nav Background Image */
				'fl-nav-bg-image'          => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Nav Background Repeat */
				'fl-nav-bg-repeat'         => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Nav Background Position */
				'fl-nav-bg-position'       => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Nav Background Attachment */
				'fl-nav-bg-attachment'     => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Nav Background Size */
				'fl-nav-bg-size'           => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),

				/* Nav Link Color */
				'fl-nav-link-color'        => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Link Color', 'tema' ),
					),
				),

				/* Nav Hover Color */
				'fl-nav-hover-color'       => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),

				/* Nav Font Family */
				'fl-nav-font-family'       => array(
					'setting' => array(
						'default'   => 'Helvetica',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Family', 'tema' ),
						'type'    => 'font',
						'connect' => 'fl-nav-font-weight',
					),
				),

				/* Nav Font Weight */
				'fl-nav-font-weight'       => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Weight', 'tema' ),
						'type'    => 'font-weight',
						'connect' => 'fl-nav-font-family',
					),
				),

				/* Nav Font Format */
				'fl-nav-font-format'       => array(
					'setting' => array(
						'default'   => 'none',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Font Format', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'       => __( 'Regular', 'tema' ),
							'capitalize' => __( 'Capitalize', 'tema' ),
							'uppercase'  => __( 'Uppercase', 'tema' ),
							'lowercase'  => __( 'Lowercase', 'tema' ),
						),
					),
				),

				/* Nav Font Size */
				'fl-nav-font-size'         => array(
					'setting' => array(
						'default'   => '16',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Size', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 10,
							'max'  => 25,
							'step' => 1,
						),
					),
				),
			),
		),
	),
));
