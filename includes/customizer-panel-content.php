<?php

/* Content Panel */
FLCustomizer::add_panel('fl-content', array(
	'title'    => _x( 'Content', 'Customizer panel title.', 'tema' ),
	'sections' => array(

		/* Content Background Section */
		'fl-content-bg'       => array(
			'title'   => _x( 'Content Background', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Content Background Color */
				'fl-content-bg-color'      => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Content Background Opacity */
				'fl-content-bg-opacity'    => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity (%)', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Content Background Image */
				'fl-content-bg-image'      => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Content Background Repeat */
				'fl-content-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Content Background Position */
				'fl-content-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Content Background Attachment */
				'fl-content-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Content Background Size */
				'fl-content-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),
			),
		),

		/* Blog Section */
		'fl-content-blog'     => array(
			'title'   => _x( 'Blog Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Blog Layout */
				'fl-blog-layout'                      => array(
					'setting' => array(
						'default' => 'sidebar-right',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Sidebar Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'sidebar-right' => __( 'Sidebar Right', 'tema' ),
							'sidebar-left'  => __( 'Sidebar Left', 'tema' ),
							'no-sidebar'    => __( 'No Sidebar', 'tema' ),
						),
					),
				),

				/* Blog Sidebar Size */
				'fl-blog-sidebar-size'                => array(
					'setting' => array(
						'default' => '4',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Sidebar Size', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'4'      => _x( 'Large', 'Sidebar size.', 'tema' ),
							'3'      => _x( 'Medium', 'Sidebar size.', 'tema' ),
							'2'      => _x( 'Small', 'Sidebar size.', 'tema' ),
							'custom' => _x( 'Custom', 'Sidebar size.', 'tema' ),
						),
					),
				),

				/* Custom Blog Sidebar Size */
				'fl-blog-custom-sidebar-size'         => array(
					'setting' => array(
						'default'           => '25',
						'sanitize_callback' => 'FLCustomizer::sanitize_number',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Custom Sidebar Width', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 10,
							'max'  => 50,
							'step' => 1,
						),
					),
				),

				/* Blog Sidebar Display */
				'fl-blog-sidebar-display'             => array(
					'setting' => array(
						'default' => 'desktop',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Sidebar Display', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'desktop' => __( 'Desktop Only', 'tema' ),
							'always'  => __( 'Always', 'tema' ),
						),
					),
				),

				/* Blog Sidebar Location */
				'fl-blog-sidebar-location'            => array(
					'setting' => array(
						'default'           => 'single,blog,search,archive',
						'sanitize_callback' => 'FLCustomizer::sanitize_checkbox_multiple',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Sidebar Location', 'tema' ),
						'type'    => 'checkbox-multiple',
						'choices' => array(
							'blog'    => __( 'Blog', 'tema' ),
							'single'  => __( 'Single Post', 'tema' ),
							'search'  => __( 'Search page', 'tema' ),
							'archive' => __( 'Archives', 'tema' ),
						),
					),
				),

				/* Enable / Disable Sidebar for Post Types */
				'fl-blog-sidebar-location-post-types' => array(
					'setting' => array(
						'default'           => 'all',
						'sanitize_callback' => 'FLCustomizer::sanitize_checkbox_multiple',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Apply Sidebar To Post Types', 'tema' ),
						'type'    => 'checkbox-multiple',
						'choices' => array(
							'custom' => 'post_types',
						),
					),
				),

				/* Line */
				'fl-blog-line1'                       => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Post Author */
				'fl-blog-post-author'                 => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Post Author', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Post Date */
				'fl-blog-post-date'                   => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Post Date', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Comment Count */
				'fl-blog-comment-count'               => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Comment Count', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),
			),
		),

		/* Archive Pages Section */
		'fl-content-archives' => array(
			'title'   => _x( 'Archive Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Show Full Text */
				'fl-archive-show-full'     => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Show Full Text', 'tema' ),
						'description' => __( 'Whether or not to show the full post. If no, the excerpt will be shown.', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'1' => __( 'Yes', 'tema' ),
							'0' => __( 'No', 'tema' ),
						),
					),
				),

				/* Read More Text */
				'fl-archive-readmore-text' => array(
					'setting' => array(
						'default' => __( 'Read More', 'tema' ),
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( '"Read More" Text', 'tema' ),
						'type'  => 'text',
					),
				),

				/* Featured Image */
				'fl-archive-show-thumbs'   => array(
					'setting' => array(
						'default' => 'beside',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Featured Image', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							''            => __( 'Hidden', 'tema' ),
							'above-title' => __( 'Above Titles', 'tema' ),
							'above'       => __( 'Above Posts', 'tema' ),
							'beside'      => __( 'Beside Posts', 'tema' ),
						),
					),
				),

				/* Featured Image Size */
				'fl-archive-thumb-size'    => array(
					'setting' => array(
						'default' => 'large',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Image Size', 'tema' ),
						'type'    => 'select',
						'choices' => archive_post_image_sizes(),
					),
				),

			),
		),

		/* Post Pages Section */
		'fl-content-posts'    => array(
			'title'   => _x( 'Post Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Featured Image */
				'fl-posts-show-thumbs' => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Featured Image', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							''            => __( 'Hidden', 'tema' ),
							'above-title' => __( 'Above Title', 'tema' ),
							'above'       => __( 'Above Post', 'tema' ),
							'beside'      => __( 'Beside Post', 'tema' ),
						),
					),
				),

				/* Featured Image Size */
				'fl-posts-thumb-size'  => array(
					'setting' => array(
						'default' => 'thumbnail',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Image Size', 'tema' ),
						'type'    => 'select',
						'choices' => single_post_image_sizes(),
					),
				),

				/* Post Categories */
				'fl-posts-show-cats'   => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Post Categories', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Post Tags */
				'fl-posts-show-tags'   => array(
					'setting' => array(
						'default' => 'visible',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Post Tags', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Prev/Next Post Links */
				'fl-posts-show-nav'    => array(
					'setting' => array(
						'default' => 'hidden',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Prev/Next Post Links', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Author Box */
				'fl-post-author-box'   => array(
					'setting' => array(
						'default' => 'hidden',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Author Box', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

			),
		),

		/* WooCommerce Section */
		'fl-content-woo'      => array(
			'title'   => _x( 'WooCommerce Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* WooCommerce Layout */
				'fl-woo-layout'              => array(
					'setting' => array(
						'default' => 'no-sidebar',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Sidebar Position', 'tema' ),
						'description' => __( 'The location of the WooCommerce sidebar.', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'sidebar-right' => __( 'Sidebar Right', 'tema' ),
							'sidebar-left'  => __( 'Sidebar Left', 'tema' ),
							'no-sidebar'    => __( 'No Sidebar', 'tema' ),
						),
					),
				),

				/* WooCommerce Sidebar Size */
				'fl-woo-sidebar-size'        => array(
					'setting' => array(
						'default' => '4',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Sidebar Size', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'4'      => _x( 'Large', 'Sidebar size.', 'tema' ),
							'3'      => _x( 'Medium', 'Sidebar size.', 'tema' ),
							'2'      => _x( 'Small', 'Sidebar size.', 'tema' ),
							'custom' => _x( 'Custom', 'Sidebar size.', 'tema' ),
						),
					),
				),

				/* Custom WooCommerce Sidebar Size */
				'fl-woo-custom-sidebar-size' => array(
					'setting' => array(
						'default'           => '25',
						'sanitize_callback' => 'FLCustomizer::sanitize_number',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Custom Sidebar Width', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 10,
							'max'  => 50,
							'step' => 1,
						),
					),
				),

				/* WooCommerce Sidebar Display */
				'fl-woo-sidebar-display'     => array(
					'setting' => array(
						'default' => 'desktop',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Sidebar Display', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'desktop' => __( 'Desktop Only', 'tema' ),
							'always'  => __( 'Always', 'tema' ),
						),
					),
				),

				/* WooCommerce Sidebar Location */
				'fl-woo-sidebar-location'    => array(
					'setting' => array(
						'default'           => 'single,shop',
						'sanitize_callback' => 'FLCustomizer::sanitize_checkbox_multiple',
					),
					'control' => array(
						'class'       => 'FLCustomizerControl',
						'label'       => __( 'Sidebar Location', 'tema' ),
						'description' => __( 'WooCommerce pages that you want sidebar to appear.', 'tema' ),
						'type'        => 'checkbox-multiple',
						'choices'     => array(
							'single'  => __( 'Single Product', 'tema' ),
							'shop'    => __( 'Shop Page', 'tema' ),
							'archive' => __( 'Categories', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-woo-line1'               => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Number of Columns */
				'fl-woo-columns'             => array(
					'setting' => array(
						'default' => '4',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Number of Columns', 'tema' ),
						'description' => __( 'Select how many columns on product category pages?', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'1' => __( '1 Column', 'tema' ),
							'2' => __( '2 Columns', 'tema' ),
							'3' => __( '3 Columns', 'tema' ),
							'4' => __( '4 Columns', 'tema' ),
							'5' => __( '5 Columns', 'tema' ),
							'6' => __( '6 Columns', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-woo-line2'               => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Number of Columns */
				'fl-woo-gallery'             => array(
					'setting' => array(
						'default'   => 'enabled',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Product Gallery', 'tema' ),
						'description' => __( 'Select how product galleries are handled.', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'enabled' => __( 'Use WooCommerce 3.x Gallery (default)', 'tema' ),
							'none'    => __( 'Disabled', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-woo-line3'               => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Add to Cart Button */
				'fl-woo-cart-button'         => array(
					'setting' => array(
						'default' => 'hidden',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( '"Add to Cart" Button', 'tema' ),
						'description' => __( 'Show the "Add to Cart" button on product category pages?', 'tema' ),
						'type'        => 'select',
						'choices'     => array(
							'visible' => __( 'Visible', 'tema' ),
							'hidden'  => __( 'Hidden', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-woo-line4'               => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* WooCommerce CSS */
				'fl-woo-css'                 => array(
					'setting' => array(
						'default' => 'enabled',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'WooCommerce Styling', 'tema' ),
						'description' => __( "Enable or disable the theme's custom WooCommerce styles.", "tema" ),
						'type'        => 'select',
						'choices'     => array(
							'enabled'  => __( 'Enabled', 'tema' ),
							'disabled' => __( 'Disabled', 'tema' ),
						),
					),
				),
			),
		),

		/* Lightbox Section */
		'fl-lightbox-layout'  => array(
			'title'   => _x( 'Lightbox', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Lightbox */
				'fl-lightbox' => array(
					'setting' => array(
						'default' => 'enabled',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Lightbox', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'enabled'  => __( 'Enabled', 'tema' ),
							'disabled' => __( 'Disabled', 'tema' ),
						),
					),
				),
			),
		),
	),
));
