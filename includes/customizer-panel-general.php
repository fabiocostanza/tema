<?php

/* General Panel */
FLCustomizer::add_panel('fl-general', array(
	'title'    => _x( 'General', 'Customizer panel title.', 'tema' ),
	'sections' => array(

		/* Layout Section */
		'fl-layout'       => array(
			'title'   => _x( 'Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Layout Width */
				'fl-layout-width'        => array(
					'setting' => array(
						'default' => 'full-width',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Width', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'boxed'      => __( 'Boxed', 'tema' ),
							'full-width' => __( 'Full Width', 'tema' ),
						),
					),
				),

				/* Content Width */
				'fl-content-width'       => array(
					'setting' => array(
						'default' => '1020',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Content Width', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 960,
							'max'  => 1920,
							'step' => 1,
						),
					),
				),

				/* Spacing */
				'fl-layout-spacing'      => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Spacing', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 150,
							'step' => 1,
						),
					),
				),

				/* Drop Shadow Size */
				'fl-layout-shadow-size'  => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Drop Shadow Size', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 75,
							'step' => 1,
						),
					),
				),

				/* Drop Shadow Color */
				'fl-layout-shadow-color' => array(
					'setting' => array(
						'default' => '#d9d9d9',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Drop Shadow Color', 'tema' ),
					),
				),

				/* Scroll To Top Button */
				'fl-scroll-to-top'       => array(
					'setting' => array(
						'default' => 'disable',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Scroll To Top Button', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'enable'  => __( 'Enabled', 'tema' ),
							'disable' => __( 'Disabled', 'tema' ),
						),
					),
				),
				/* Framework */
				'fl-framework'           => array(
					'setting' => array(
						'default' => 'base',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'CSS Framework', 'tema' ),
						'type'        => 'select',
						'description' =>
						__( 'Select a CSS framework for the theme. Default is a bare minimal Bootstrap 3.', 'tema' ),
						'choices'     => array(
							'base'        => __( 'Minimal Bootstrap 3', 'tema' ),
							'base-4'      => __( 'Minimal Bootstrap 4', 'tema' ),
							'bootstrap'   => __( 'Full Bootstrap 3', 'tema' ),
							'bootstrap-4' => __( 'Full Bootstrap 4', 'tema' ),
						),
					),
				),

				/* Font Awesome */
				'fl-awesome'             => array(
					'setting' => array(
						'default' => 'none',
					),
					'control' => array(
						'class'       => 'WP_Customize_Control',
						'label'       => __( 'Font Awesome Icons', 'tema' ),
						'type'        => 'select',
						'description' =>
						__( 'Select which icon library to load on all pages. If unsure choose None.', 'tema' ),
						'choices'     => array(
							'none' => __( 'None', 'tema' ),
							'fa4'  => __( 'Font Awesome 4 Shim', 'tema' ),
							'fa5'  => __( 'Font Awesome 5', 'tema' ),
						),
					),
				),
				/* Medium Breakpoint */
				'fl-medium-breakpoint'      => array(
					'setting' => array(
						'default' => 992,
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Theme Medium Breakpoint', 'tema' ),
						'description' => __( 'Medium device behavior starts below this setting.', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 500,
							'max'  => 1200,
							'step' => 1,
						),
					),
				),

				/* Mobile Breakpoint */
				'fl-mobile-breakpoint'      => array(
					'setting' => array(
						'default' => 768,
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Theme Mobile Breakpoint', 'tema' ),
						'description' => __( 'Mobile device behavior starts below this setting.', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 500,
							'max'  => 1200,
							'step' => 1,
						),
					),
				),
			),
		),

		/* Body Background Section */
		'fl-body-bg'      => array(
			'title'   => _x( 'Background', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Background Color */
				'fl-body-bg-color'      => array(
					'setting' => array(
						'default' => '#f2f2f2',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Background Image */
				'fl-body-bg-image'      => array(
					'setting' => array(
						'default'   => '',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Background Repeat */
				'fl-body-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Background Position */
				'fl-body-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Background Attachment */
				'fl-body-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Background Size */
				'fl-body-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),
			),
		),

		/* Accent Color Section */
		'fl-accent-color' => array(
			'title'   => _x( 'Accent Color', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Accent Color */
				'fl-accent'       => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class'       => 'WP_Customize_Color_Control',
						'label'       => __( 'Color', 'tema' ),
						'description' => __( 'The accent color will be used to color elements such as links and buttons as well as various elements in your theme.', 'tema' ),
					),
				),

				/* Accent Hover Color */
				'fl-accent-hover' => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
			),
		),

		/* Heading Font Section */
		'fl-heading-font' => array(
			'title'   => _x( 'Headings', 'Customizer section title.', 'tema' ),
			'options' => array(

				'fl-heading-style'       => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class'    => 'WP_Customize_Control',
						'type'     => 'select',
						'label'    => __( 'Style Headings', 'tema' ),
						'priority' => 0,
						'choices'  => array(
							''      => __( 'All Headings', 'tema' ),
							'title' => __( 'Custom H1 Style', 'tema' ),
						),
					),
				),

				'fl-title-text-color'    => array(
					'setting' => array(
						'default' => '#333333',
					),
					'control' => array(
						'class'    => 'WP_Customize_Color_Control',
						'label'    => __( 'H1 Color', 'tema' ),
						'priority' => 1.0,
					),
				),
				'fl-title-font-family'   => array(
					'setting' => array(
						'default' => 'Helvetica',
					),
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'label'    => __( 'H1 Font Family', 'tema' ),
						'priority' => 1.1,
						'type'     => 'font',
						'connect'  => 'fl-title-font-weight',
					),
				),
				/* Heading Font Weight */
				'fl-title-font-weight'   => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'label'    => __( 'H1 Font Weight', 'tema' ),
						'type'     => 'font-weight',
						'priority' => 1.2,
						'connect'  => 'fl-title-font-family',
					),
				),
				/* Heading Font Format */
				'fl-title-font-format'   => array(
					'setting' => array(
						'default' => 'none',
					),
					'control' => array(
						'class'    => 'WP_Customize_Control',
						'label'    => __( 'H1 Font Format', 'tema' ),
						'priority' => 1.3,
						'type'     => 'select',
						'choices'  => array(
							'none'       => __( 'Regular', 'tema' ),
							'capitalize' => __( 'Capitalize', 'tema' ),
							'uppercase'  => __( 'Uppercase', 'tema' ),
							'lowercase'  => __( 'Lowercase', 'tema' ),
						),
					),
				),

				/* Below Title Styles */
				'fl-title-heading-line'  => array(
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'type'     => 'line',
						'priority' => 2.0,
					),
				),

				/* Heading Text Color */
				'fl-heading-text-color'  => array(
					'setting' => array(
						'default' => '#333333',
					),
					'control' => array(
						'class'    => 'WP_Customize_Color_Control',
						'label'    => __( 'Color', 'tema' ),
						'priority' => 3.0,
					),
				),

				/* Heading Font Family */
				'fl-heading-font-family' => array(
					'setting' => array(
						'default'   => 'Helvetica',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'label'    => __( 'Font Family', 'tema' ),
						'type'     => 'font',
						'connect'  => 'fl-heading-font-weight',
						'priority' => 3.1,
					),
				),

				/* Heading Font Weight */
				'fl-heading-font-weight' => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'label'    => __( 'Font Weight', 'tema' ),
						'type'     => 'font-weight',
						'connect'  => 'fl-heading-font-family',
						'priority' => 3.2,
					),
				),

				/* Heading Font Format */
				'fl-heading-font-format' => array(
					'setting' => array(
						'default'   => 'none',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'    => 'WP_Customize_Control',
						'label'    => __( 'Font Format', 'tema' ),
						'type'     => 'select',
						'priority' => 3.3,
						'choices'  => array(
							'none'       => __( 'Regular', 'tema' ),
							'capitalize' => __( 'Capitalize', 'tema' ),
							'uppercase'  => __( 'Uppercase', 'tema' ),
							'lowercase'  => __( 'Lowercase', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-heading-font-line1'  => array(
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'type'     => 'line',
						'priority' => 4.0,
					),
				),

				/* H1 Font Size */
				'fl-h1-font-size'        => array(
					'setting' => array(
						'default'   => '36',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H1' ),
						'type'       => 'slider',
						'priority'   => 5.0,
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H1 Line Height */
				'fl-h1-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H1' ),
						'priority'   => 5.1,
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H1 Letter Spacing */
				'fl-h1-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H1' ),
						'priority'   => 5.2,
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Line */
				'fl-h1-line'             => array(
					'control' => array(
						'class'    => 'FLCustomizerControl',
						'type'     => 'line',
						'priority' => 5.3,
					),
				),

				/* H2 Font Size */
				'fl-h2-font-size'        => array(
					'setting' => array(
						'default'   => '30',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H2' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H2 Line Height */
				'fl-h2-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H2' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H2 Letter Spacing */
				'fl-h2-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H2' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Line */
				'fl-h2-line'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* H3 Font Size */
				'fl-h3-font-size'        => array(
					'setting' => array(
						'default'   => '24',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H3' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H3 Line Height */
				'fl-h3-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H3' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H3 Letter Spacing */
				'fl-h3-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H3' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Line */
				'fl-h3-line'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* H4 Font Size */
				'fl-h4-font-size'        => array(
					'setting' => array(
						'default'   => '18',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H4' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H4 Line Height */
				'fl-h4-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H4' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H4 Letter Spacing */
				'fl-h4-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H4' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Line */
				'fl-h4-line'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* H5 Font Size */
				'fl-h5-font-size'        => array(
					'setting' => array(
						'default'   => '14',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H5' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H5 Line Height */
				'fl-h5-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H5' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H5 Letter Spacing */
				'fl-h5-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H5' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Line */
				'fl-h5-line'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* H6 Font Size */
				'fl-h6-font-size'        => array(
					'setting' => array(
						'default'   => '12',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Font Size', '%s stands for HTML heading tag.', 'tema' ), 'H6' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* H6 Line Height */
				'fl-h6-line-height'      => array(
					'setting' => array(
						'default'   => '1.4',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Line Height', '%s stands for HTML heading tag.', 'tema' ), 'H6' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),

				/* H6 Letter Spacing */
				'fl-h6-letter-spacing'   => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						/* translators: %s: HTML heading tag */
						'label'      => sprintf( _x( '%s Letter Spacing', '%s stands for HTML heading tag.', 'tema' ), 'H6' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => -3,
							'max'  => 10,
							'step' => 1,
						),
						'responsive' => true,
					),
				),
			),
		),

		/* Body Font Section */
		'fl-body-font'    => array(
			'title'   => _x( 'Text', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Body Text Color */
				'fl-body-text-color'  => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Color', 'tema' ),
					),
				),

				/* Body Font Family */
				'fl-body-font-family' => array(
					'setting' => array(
						'default' => 'Helvetica',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Family', 'tema' ),
						'type'    => 'font',
						'connect' => 'fl-body-font-weight',
					),
				),

				/* Body Font Weight */
				'fl-body-font-weight' => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Weight', 'tema' ),
						'type'    => 'font-weight',
						'connect' => 'fl-body-font-family',
					),
				),

				/* Body Font Size */
				'fl-body-font-size'   => array(
					'setting' => array(
						'default'   => '14',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						'label'      => __( 'Font Size', 'tema' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
						'responsive' => true,
					),
				),

				/* Body Line Height */
				'fl-body-line-height' => array(
					'setting' => array(
						'default'   => '1.45',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'      => 'FLCustomizerControl',
						'label'      => __( 'Line Height', 'tema' ),
						'type'       => 'slider',
						'choices'    => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
						'responsive' => true,
					),
				),
			),
		),
		/* Buttons Section */
		'fl-buttons'      => array(
			'title'   => _x( 'Buttons', 'Customizer section title.', 'tema' ),
			'options' => array(
				'fl-button-style'                  => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Button Style', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							''       => __( 'None (default)', 'tema' ),
							'custom' => __( 'Custom', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-button-color-line'             => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),


				'fl-button-color'                  => array(
					'setting' => array(
						'default'   => '#808080',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Color', 'tema' ),
					),
				),
				'fl-button-hover-color'            => array(
					'setting' => array(
						'default'   => '#555',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
				'fl-button-background-color'       => array(
					'setting' => array(
						'default'   => '#fff',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),
				'fl-button-background-hover-color' => array(
					'setting' => array(
						'default'   => '#eee',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Hover Color', 'tema' ),
					),
				),

				/* Line */
				'fl-button-font-line'              => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Body Font Family */
				'fl-button-font-family'            => array(
					'setting' => array(
						'default' => 'Helvetica',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Family', 'tema' ),
						'type'    => 'font',
						'connect' => 'fl-button-font-weight',
					),
				),
				/* Body Font Weight */
				'fl-button-font-weight'            => array(
					'setting' => array(
						'default' => '400',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Font Weight', 'tema' ),
						'type'    => 'font-weight',
						'connect' => 'fl-button-font-family',
					),
				),

				'fl-button-font-size'              => array(
					'setting' => array(
						'default' => '16',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => _x( 'Font Size', 'Font size for buttons.', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 10,
							'max'  => 72,
							'step' => 1,
						),
					),
				),
				'fl-button-line-height'            => array(
					'setting' => array(
						'default' => '1.2',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Line Height', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 1,
							'max'  => 2.5,
							'step' => 0.05,
						),
					),
				),
				'fl-button-text-transform'         => array(
					'setting' => array(
						'default'   => 'none',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => _x( 'Text Transform', 'Text transform for buttons.', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'       => __( 'Regular', 'tema' ),
							'capitalize' => __( 'Capitalize', 'tema' ),
							'uppercase'  => __( 'Uppercase', 'tema' ),
							'lowercase'  => __( 'Lowercase', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-button-border-line'            => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),
				'fl-button-border-style'           => array(
					'setting' => array(
						'default'   => 'none',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => _x( 'Border Style', 'Border style for buttons.', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'    => __( 'None', 'tema' ),
							'solid'   => __( 'Solid', 'tema' ),
							'dotted'  => __( 'Dotted', 'tema' ),
							'dashed'  => __( 'Dashed', 'tema' ),
							'double'  => __( 'Double', 'tema' ),
							'groove'  => __( 'Groove', 'tema' ),
							'ridge'   => __( 'Ridge', 'tema' ),
							'inset'   => __( 'Inset', 'tema' ),
							'outset'  => __( 'Outset', 'tema' ),
							'initial' => __( 'Initial', 'tema' ),
							'inherit' => __( 'Inherit', 'tema' ),
						),
					),
				),
				'fl-button-border-width'           => array(
					'setting' => array(
						'default'   => '0',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => _x( 'Border Width', 'Border width for buttons.', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 10,
							'step' => 1,
						),
					),
				),
				'fl-button-border-color'           => array(
					'setting' => array(
						'default'   => '',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Border Color', 'tema' ),
					),
				),
				'fl-button-border-radius'          => array(
					'setting' => array(
						'default'   => 0,
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => _x( 'Border Radius', 'Font size for buttons.', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 25,
							'step' => 1,
						),
					),
				),
			),
		),
		/* Social Links Section */
		'fl-social-links' => array(
			'title'   => _x( 'Social Links', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Social Icons Color */
				'fl-social-icons-color'       => array(
					'setting' => array(
						'default' => 'mono',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Social Icons Color', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'branded' => __( 'Branded', 'tema' ),
							'mono'    => __( 'Monochrome', 'tema' ),
							'custom'  => __( 'Custom', 'tema' ),
						),
					),
				),

				/* Social Icons bg Shape */
				'fl-social-icons-bg-shape'    => array(
					'setting' => array(
						'default' => 'circle',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Shape', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'circle' => __( 'Round', 'tema' ),
							'square' => __( 'Square', 'tema' ),
						),
					),
				),

				/* Social Icons bg Color */
				'fl-social-icons-bg-color'    => array(
					'setting' => array(
						'default' => '#000',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Social Icons fg Color */
				'fl-social-icons-fg-color'    => array(
					'setting' => array(
						'default' => '#FFF',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Icon Color', 'tema' ),
					),
				),

				/* Social Icons hover Color */
				'fl-social-icons-hover-color' => array(
					'setting' => array(
						'default' => '#666',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),

				/* Drop Shadow Size */
				'fl-social-icons-size'        => array(
					'setting' => array(
						'default' => '2',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Icon Size', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 1,
							'max'  => 8,
							'step' => 1,
						),
					),
				),



				/* Social Links (no need to translate brand names) */
				'fl-social-facebook'          => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Facebook',
					),
				),
				'fl-social-twitter'           => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Twitter',
					),
				),
				'fl-social-google'            => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Google',
					),
				),
				'fl-social-snapchat'          => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Snapchat',
					),
				),
				'fl-social-linkedin'          => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'LinkedIn',
					),
				),
				'fl-social-yelp'              => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Yelp',
					),
				),
				'fl-social-xing'              => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Xing',
					),
				),
				'fl-social-pinterest'         => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Pinterest',
					),
				),
				'fl-social-tumblr'            => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Tumblr',
					),
				),
				'fl-social-vimeo'             => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Vimeo',
					),
				),
				'fl-social-youtube'           => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'YouTube',
					),
				),
				'fl-social-flickr'            => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Flickr',
					),
				),
				'fl-social-instagram'         => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Instagram',
					),
				),
				'fl-social-skype'             => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Skype',
					),
				),
				'fl-social-dribbble'          => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Dribbble',
					),
				),
				'fl-social-500px'             => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => '500px',
					),
				),
				'fl-social-blogger'           => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'Blogger',
					),
				),
				'fl-social-github'            => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => 'GitHub',
					),
				),
				'fl-social-rss'               => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( 'RSS', 'tema' ),
					),
				),
				'fl-social-email'             => array(
					'control' => array(
						'class' => 'WP_Customize_Control',
						'label' => __( 'Email', 'tema' ),
					),
				),
			),
		),
	),
));
