<?php

/* Footer Panel */
FLCustomizer::add_panel('fl-footer', array(
	'title'    => _x( 'Footer', 'Customizer panel title.', 'tema' ),
	'sections' => array(

		/* Footer Widgets Layout Section */
		'fl-footer-widgets-layout' => array(
			'title'   => _x( 'Footer Widgets Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Footer Widgets Display */
				'fl-footer-widgets-display' => array(
					'setting' => array(
						'default' => 'all',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Footer Widgets Display', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'disabled' => __( 'Disabled', 'tema' ),
							'all'      => __( 'All Pages', 'tema' ),
							'home'     => __( 'Homepage Only', 'tema' ),
						),
					),
				),
			),
		),

		/* Footer Widgets Style Section */
		'fl-footer-widgets-style'  => array(
			'title'   => _x( 'Footer Widgets Style', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Footer Widgets Background Color */
				'fl-footer-widgets-bg-color'      => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Footer Widgets Background Opacity */
				'fl-footer-widgets-bg-opacity'    => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Footer Widgets Background Gradient */
				'fl-footer-widgets-bg-gradient'   => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Gradient', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'0' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'1' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Footer Widgets Background Image */
				'fl-footer-widgets-bg-image'      => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Footer Widgets Background Repeat */
				'fl-footer-widgets-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Footer Widgets Background Position */
				'fl-footer-widgets-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Footer Widgets Background Attachment */
				'fl-footer-widgets-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Footer Widgets Background Size */
				'fl-footer-widgets-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),

				/* Footer Widgets Text Color */
				'fl-footer-widgets-text-color'    => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Text Color', 'tema' ),
					),
				),

				/* Footer Widgets Link Color */
				'fl-footer-widgets-link-color'    => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Link Color', 'tema' ),
					),
				),

				/* Footer Widgets Hover Color */
				'fl-footer-widgets-hover-color'   => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
			),
		),

		/* Footer Layout Section */
		'fl-footer-layout'         => array(
			'title'   => _x( 'Footer Layout', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Footer Layout */
				'fl-footer-layout'      => array(
					'setting' => array(
						'default' => '1-col',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Layout', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'none'   => __( 'None', 'tema' ),
							'1-col'  => __( '1 Column', 'tema' ),
							'2-cols' => __( '2 Columns', 'tema' ),
						),
					),
				),

				/* Line */
				'fl-footer-line1'       => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Footer Column 1 Layout */
				'fl-footer-col1-layout' => array(
					'setting' => array(
						'default' => 'text',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label'   => sprintf( _x( 'Column %d Layout', '%d stands for column order number.', 'tema' ), 1 ),
						'type'    => 'select',
						'choices' => array(
							'text'        => __( 'Text', 'tema' ),
							'social'      => __( 'Social Icons', 'tema' ),
							'social-text' => __( 'Text &amp; Social Icons', 'tema' ),
							'menu'        => __( 'Menu', 'tema' ),
						),
					),
				),

				/* Footer Column 1 Text */
				'fl-footer-col1-text'   => array(
					'setting' => array(
						'default'   => '',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label' => sprintf( _x( 'Column %d Text', '%d stands for column order number.', 'tema' ), 1 ),
						'type'  => 'textarea',
					),
				),

				/* Line */
				'fl-footer-line2'       => array(
					'control' => array(
						'class' => 'FLCustomizerControl',
						'type'  => 'line',
					),
				),

				/* Footer Column 2 Layout */
				'fl-footer-col2-layout' => array(
					'setting' => array(
						'default' => 'text',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label'   => sprintf( _x( 'Column %d Layout', '%d stands for column order number.', 'tema' ), 2 ),
						'type'    => 'select',
						'choices' => array(
							'text'        => __( 'Text', 'tema' ),
							'social'      => __( 'Social Icons', 'tema' ),
							'social-text' => __( 'Text &amp; Social Icons', 'tema' ),
							'menu'        => __( 'Menu', 'tema' ),
						),
					),
				),

				/* Footer Column 2 Text */
				'fl-footer-col2-text'   => array(
					'setting' => array(
						'default'   => '1-800-555-5555 &bull; <a href="mailto:info@mydomain.com">info@mydomain.com</a>',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class' => 'WP_Customize_Control',
						/* translators: %d: Column order number */
						'label' => sprintf( _x( 'Column %d Text', '%d stands for column order number.', 'tema' ), 2 ),
						'type'  => 'textarea',
					),
				),
			),
		),

		/* Footer Style Section */
		'fl-footer-style'          => array(
			'title'   => _x( 'Footer Style', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Footer Background Color */
				'fl-footer-bg-color'      => array(
					'setting' => array(
						'default' => '#ffffff',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Background Color', 'tema' ),
					),
				),

				/* Footer Background Opacity */
				'fl-footer-bg-opacity'    => array(
					'setting' => array(
						'default' => '100',
					),
					'control' => array(
						'class'   => 'FLCustomizerControl',
						'label'   => __( 'Background Opacity', 'tema' ),
						'type'    => 'slider',
						'choices' => array(
							'min'  => 0,
							'max'  => 100,
							'step' => 1,
						),
					),
				),

				/* Footer Background Gradient */
				'fl-footer-bg-gradient'   => array(
					'setting' => array(
						'default' => '0',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Gradient', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'0' => _x( 'Disabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
							'1' => _x( 'Enabled', 'Used for multiple Customizer options values. Use generalized form of the word when translating.', 'tema' ),
						),
					),
				),

				/* Footer Background Image */
				'fl-footer-bg-image'      => array(
					'setting' => array(
						'default' => '',
					),
					'control' => array(
						'class' => 'WP_Customize_Image_Control',
						'label' => __( 'Background Image', 'tema' ),
					),
				),

				/* Footer Background Repeat */
				'fl-footer-bg-repeat'     => array(
					'setting' => array(
						'default'   => 'no-repeat',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Repeat', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'no-repeat' => __( 'None', 'tema' ),
							'repeat'    => __( 'Tile', 'tema' ),
							'repeat-x'  => __( 'Horizontal', 'tema' ),
							'repeat-y'  => __( 'Vertical', 'tema' ),
						),
					),
				),

				/* Footer Background Position */
				'fl-footer-bg-position'   => array(
					'setting' => array(
						'default'   => 'center top',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Position', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'left top'      => __( 'Left Top', 'tema' ),
							'left center'   => __( 'Left Center', 'tema' ),
							'left bottom'   => __( 'Left Bottom', 'tema' ),
							'right top'     => __( 'Right Top', 'tema' ),
							'right center'  => __( 'Right Center', 'tema' ),
							'right bottom'  => __( 'Right Bottom', 'tema' ),
							'center top'    => __( 'Center Top', 'tema' ),
							'center center' => __( 'Center', 'tema' ),
							'center bottom' => __( 'Center Bottom', 'tema' ),
						),
					),
				),

				/* Footer Background Attachment */
				'fl-footer-bg-attachment' => array(
					'setting' => array(
						'default'   => 'scroll',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Attachment', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'scroll' => __( 'Scroll', 'tema' ),
							'fixed'  => __( 'Fixed', 'tema' ),
						),
					),
				),

				/* Footer Background Size */
				'fl-footer-bg-size'       => array(
					'setting' => array(
						'default'   => 'auto',
						'transport' => 'postMessage',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Background Scale', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'auto'    => __( 'None', 'tema' ),
							'contain' => __( 'Fit', 'tema' ),
							'cover'   => __( 'Fill', 'tema' ),
						),
					),
				),

				/* Footer Text Color */
				'fl-footer-text-color'    => array(
					'setting' => array(
						'default' => '#808080',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Text Color', 'tema' ),
					),
				),

				/* Footer Link Color */
				'fl-footer-link-color'    => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Link Color', 'tema' ),
					),
				),

				/* Footer Hover Color */
				'fl-footer-hover-color'   => array(
					'setting' => array(
						'default' => '#428bca',
					),
					'control' => array(
						'class' => 'WP_Customize_Color_Control',
						'label' => __( 'Hover Color', 'tema' ),
					),
				),
			),
		),
		/* Footer Parallax */
		'fl-footer-effect'         => array(
			'title'   => _x( 'Footer Parallax', 'Customizer section title.', 'tema' ),
			'options' => array(

				/* Footer Effect  */
				'fl-footer-parallax-effect' => array(
					'setting' => array(
						'default' => 'disable',
					),
					'control' => array(
						'class'   => 'WP_Customize_Control',
						'label'   => __( 'Footer Parallax Effect', 'tema' ),
						'type'    => 'select',
						'choices' => array(
							'disable' => __( 'Disabled', 'tema' ),
							'enable'  => __( 'Enabled', 'tema' ),
						),
					),
				),
			),
		),

	),
));
