<?php

if ( class_exists( 'FLUpdater' ) ) {
	FLUpdater::add_product(array(
		'name'    => 'Theme',
		'version' => '1.7.3',
		'slug'    => 'bb-theme',
		'type'    => 'theme',
	));
}
